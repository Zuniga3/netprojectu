﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio, p.Sku + " - " + p.Nombre);
            }

           EmpleadoModel.Populate();
            foreach (Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id, emp.Inss, emp.Cedula, emp.Nombre, emp.Apellidos, emp.Direccion, emp.Tconvencional, emp.Tcelular, emp.Salario, emp.Sexo, emp.Nombre + " " + emp.Apellidos);
            }

            ClienteModel.Populate();
            foreach (Cliente c in ClienteModel.GetListCliente())
            {
                dsProductos.Tables["Cliente"].Rows.Add(c.Id, c.Cedula, c.Nombres, c.Apellidos,c.Telefono , c.Correo, c.Direccion,c.Nombres + " "+c.Apellidos );
            }
           

        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }

        private void nuevoReporteDeFacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReporteFacturaEmpleado rfe = new ReporteFacturaEmpleado();
            rfe.MdiParent = this;
            rfe.DsSistema = dsProductos;
            rfe.Show();
        }
    }
}
