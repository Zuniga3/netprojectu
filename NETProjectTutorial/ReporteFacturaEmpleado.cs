﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class ReporteFacturaEmpleado : Form
    {
        DataSet dsSistema;
        BindingSource bsSistema;

        public ReporteFacturaEmpleado()
        {
            InitializeComponent();
            bsSistema = new BindingSource();
        }

        public DataSet DsSistema
        {
            get
            {
                return dsSistema;
            }

            set
            {
                dsSistema = value;
            }
        }

        private void ReporteFacturaEmpleado_Load(object sender, EventArgs e)
        {
            bsSistema.DataSource = DsSistema;
            bsSistema.DataMember = DsSistema.Tables["ReporteEmpleado"].TableName;
            dgvReporte.DataSource = bsSistema;
            dgvReporte.AutoGenerateColumns = true;
            cmbFilter.DataSource = dsSistema.Tables["Factura"];
            cmbFilter.DisplayMember = "Empleado";
            cmbFilter.ValueMember = "id";
        }
    }
}
