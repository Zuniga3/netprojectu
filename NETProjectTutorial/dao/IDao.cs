﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial
{
    interface IDao <T>
    {
        void save(T t);
        void update(T t);
        bool delete(T t);
        List<T> findAll();

    }
}
