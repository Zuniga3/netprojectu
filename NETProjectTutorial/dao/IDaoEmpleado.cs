﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.dao
{
    interface IDaoEmpleado : IDao<Empleado>
    {
        Empleado findById(int id);

        List<Empleado> findByLastName(string lastname);

    }
}
