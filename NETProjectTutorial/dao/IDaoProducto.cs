﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoProducto : IDao<Producto>
    {
        Producto findById(int id);

        List<Producto> findBySKUN(string sku);


    }
}
