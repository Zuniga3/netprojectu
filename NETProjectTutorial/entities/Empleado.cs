﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id;// 1 => 4
        private string nombre;// 20 => 43
        private string apellidos;// 20 => 43
        private string cedula;// 16 => 35
        private string inss;// 16 => 35
        private string direccion;// 100 => 203
        private double salario;// 16 => 35
        private string tconvencional; // 16 => 35
        private string tcelular; // 16 => 35
        private SEXO sexo; // 8 => 19 

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        }

        public string Tconvencional
        {
            get
            {
                return tconvencional;
            }

            set
            {
                tconvencional = value;
            }
        }

        public string Tcelular
        {
            get
            {
                return tcelular;
            }

            set
            {
                tcelular = value;
            }
        }

        internal SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public Empleado() { }

        public Empleado(int id, string nombre, string apellidos, string cedula, string inss, string direccion, double salario, string tconvencional, string tcelular, SEXO sexo)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Apellidos = apellidos;
            this.Cedula = cedula;
            this.Inss = inss;
            this.Direccion = direccion;
            this.Salario = salario;
            this.Tconvencional = tconvencional;
            this.Tcelular = tcelular;
            this.Sexo = sexo;
        }

        public override string ToString()
        {
            return nombre + " " + apellidos;
        }

        public enum SEXO
        {
            FEMALE, MALE
        }
    }
}
