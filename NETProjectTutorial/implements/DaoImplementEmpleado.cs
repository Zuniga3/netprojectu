﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NETProjectTutorial.entities.Empleado;

namespace NETProjectTutorial.implements
{
    class DaoImplementEmpleado : IDaoEmpleado
    {
        private BinaryReader brhEmpleado;
        private BinaryWriter bwhEmpleado;
        private BinaryReader brdEmpleado;
        private BinaryWriter bwdEmpleado;

        private FileStream fshEmpleado;
        private FileStream fsdEmpleado;

        private const string FILENAME_HEADER = "hEmpleado.dat";
        private const string FILENAME_DATA = "dEmpleado.dat";
        private const int SIZE = 417;

        private void open()
        {
            try
            {
                fsdEmpleado = new FileStream(FILENAME_DATA,FileMode.OpenOrCreate, FileAccess.ReadWrite );

                if (!File.Exists(FILENAME_HEADER))
                {
                    fshEmpleado = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado = new BinaryWriter(fshEmpleado);
                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);
                    bwhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhEmpleado.Write(0);
                    bwhEmpleado.Write(0);
                }
                else
                {
                    fshEmpleado = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado = new BinaryWriter(fshEmpleado);
                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);
                }
               
            }
            catch(IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        private void close()
        {
            try
            {
                if (brdEmpleado != null)
                {
                    brdEmpleado.Close();
                }
                if (brhEmpleado != null)
                {
                    brhEmpleado.Close();
                }
                if (bwdEmpleado != null)
                {
                    bwdEmpleado.Close();
                }
                if (brdEmpleado != null)
                {
                    brdEmpleado.Close();
                }
                if (fsdEmpleado != null)
                {
                    fsdEmpleado.Close();
                }
                if (fshEmpleado  != null)
                {
                    fshEmpleado.Close();
                }
            }
            catch (IOException e)
            {

                throw new IOException(e.Message);
            }
        }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> empleados = new List<Empleado>();

            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //Calculamos posición cabecera
                long hpos = 8 + i * 4;
                brhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhEmpleado.ReadInt32();

                //Calculamos posición de los datos
                long dpos = (index - 1) * SIZE;
                brdEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdEmpleado.ReadInt32();
                string nombre = brdEmpleado.ReadString();
                string apellidos = brdEmpleado.ReadString();
                string cedula = brdEmpleado.ReadString();
                string inss = brdEmpleado.ReadString();
                string dirreccion = brdEmpleado.ReadString();
                double salario = brdEmpleado.ReadDouble();
                string tconvencional = brdEmpleado.ReadString();
                string celular = brdEmpleado.ReadString();
                SEXO sexo = (SEXO)brdEmpleado.Read();
                Empleado t = new Empleado(id, nombre, apellidos, cedula, inss, dirreccion, salario,
                    tconvencional, celular, sexo);
                empleados.Add(t);
            }

            close();
            return empleados;
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastName(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            throw new NotImplementedException();
        }

        public void update(Empleado t)
        {
            throw new NotImplementedException();
        }
    }
}
