﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;

        public ClienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }

        public static List<Cliente> GetListCliente()
        {
            return ListClientes;
        }

        public static void Populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_data));
           
        }

        public void save(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cédula"].ToString();
            c.Nombres = cliente["Nombres"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Teléfono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Direccion"].ToString();

            daocliente.save(c);
        }

        public void delate( DataRow Cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(Cliente["Id"].ToString());
            c.Cedula = Cliente["Cedula"].ToString();
            c.Nombres = Cliente["Nombres"].ToString();
            c.Apellidos = Cliente["Apellidos"].ToString();
            c.Telefono = Cliente["Telefono"].ToString();
            c.Correo = Cliente["Correo"].ToString();
            c.Direccion = Cliente["Direccion"].ToString();

            daocliente.delete(c);
        }

        public void update(DataRow Cliente)
        {
            Cliente  c = new Cliente();
            c.Id = Convert.ToInt32(Cliente["Id"].ToString());
            c.Cedula = Cliente["Cedula"].ToString();
            c.Nombres = Cliente["Nombres"].ToString();
            c.Apellidos = Cliente["Apellidos"].ToString();
            c.Telefono = Cliente["Telefono"].ToString();
            c.Correo = Cliente["correo"].ToString();
            c.Direccion = Cliente["Direccion"].ToString();

            daocliente.update(c);
        }


    }
}
