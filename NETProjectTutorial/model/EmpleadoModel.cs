﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.implements;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementEmpleado daoEmpleados;

        public EmpleadoModel()
        {
            daoEmpleados = new implements.DaoImplementEmpleado();
        }

        public static List<Empleado> GetListEmpleado()
        {
            return ListEmpleados;
        }

        public static void Populate()
        {
            /*Empleado[] empleados =
            {
                new Empleado(1, "Pepito", "Pérez", "001-260201-1025V", "1658574-2", "Del arbolito 2c. abajo", 10000, "22528274", "81615721", Empleado.SEXO.MASCULINO),
                new Empleado(2, "Ana", "Conda", "001-260201-1025F", "1785965-2", "Del arbolito 2c. abajo", 50000, "22528274", "83264848", Empleado.SEXO.FEMENINO),
                new Empleado(3, "Juan", "Camaney", "001-260201-1025J", "235689-2", "Del arbolito 2c. abajo", 10000, "22528274", "85987654", Empleado.SEXO.MASCULINO)
            };*/

            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));
            
        }

        }
}
